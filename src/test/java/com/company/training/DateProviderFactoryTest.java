package com.company.training;

import com.company.training.dateProviders.DateProviderFactory;
import com.company.training.dateProviders.DateProviderType;
import com.company.training.dateProviders.DayProvider;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;

public class DateProviderFactoryTest {

    @Test
    public void Test() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        var obj = new DateProviderFactory();
        var actual = obj.createInstance(DateProviderType.Day);
        var expected = DayProvider.class;
        Assert.assertTrue(expected.isInstance(actual));

    }

}