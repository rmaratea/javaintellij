package com.company.training.dateProviders;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DayTimeProvider {

    public String getToday() {
        var dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        var now = LocalDateTime.now();
        return dtf.format(now);
    }
}
