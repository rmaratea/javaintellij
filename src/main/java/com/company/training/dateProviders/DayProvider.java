package com.company.training.dateProviders;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

public class DayProvider {

    public String getToday() {
        var dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        var now = LocalDateTime.now();
        return dtf.format(now);
    }
}
