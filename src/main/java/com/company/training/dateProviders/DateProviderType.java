package com.company.training.dateProviders;

public enum DateProviderType {
    Day("Day"), DayTime("DayTime");

    private String description;

    DateProviderType(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}


