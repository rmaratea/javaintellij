package com.company.training.dateProviders;

import com.company.common.Factory;

import java.util.HashMap;

public class DateProviderFactory extends Factory<DateProviderType, DayProvider> {

    @Override
    protected HashMap<DateProviderType, DayProvider> populateMap(HashMap<DateProviderType, DayProvider> emptyMap) {
        emptyMap.put(DateProviderType.Day, new DayProvider());
        emptyMap.put(DateProviderType.DayTime, new DayProvider());

        return emptyMap;
    }

}