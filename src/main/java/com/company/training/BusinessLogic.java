package com.company.training;

import com.company.training.dateProviders.DateProviderFactory;
import com.company.training.dateProviders.DateProviderType;
import com.company.training.dateProviders.DayProvider;

import java.lang.reflect.InvocationTargetException;

public class BusinessLogic {

    //region ============================= Properties =============================
    private DateProviderFactory _dateProviderFactory;

    public void setDateProviderFactory(DateProviderFactory dateProviderFactory) {
        this._dateProviderFactory = dateProviderFactory;
    }

    public DateProviderFactory getDateProviderFactory() {
        if (_dateProviderFactory == null)
            _dateProviderFactory = new DateProviderFactory();

        return _dateProviderFactory;
    }

    //endregion

    public void run() throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        var obj = this.getDateProviderFactory().createInstance(DateProviderType.Day);
        System.out.println(obj.getToday());
    }

}
