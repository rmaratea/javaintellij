package com.company;

import com.company.training.BusinessLogic;

import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//        runApp();
        runAppCalculator();

    }

    private static void runAppCalculator() {

       /* Console console = System.console();
        String input = console.readLine("Enter input:");*/
        var value1 = getValue("Enter input 1:");
        var value2 = getValue("Enter input 2:");

        System.out.println(value1 / value2);

    }

    private static double getValue(String s) {
        System.out.println(s);
        var scanner = new Scanner(System.in); // Write this in your code once
        return scanner.nextDouble(); // Then use scanner.nextLine as a substitute for console.readLine
    }

    private static void runApp() {
        try {
            var core = new BusinessLogic();
            core.run();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

}
