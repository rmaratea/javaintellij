package com.company.common;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 * Abstract class that can be base for by any factory pattern implementation
 * @param <TInput>: Type of the class type identifier
 * @param <TOutput>Class type to be instantiated
 */
public abstract class Factory<TInput, TOutput> {
    protected HashMap<TInput, TOutput> _map;

    public Factory() {
        _map = initMap();
    }

    /**
     * To be implemented in order to populate the map that is storing class to be instanciated for each instanceType
     *
     * @param emptyMap: This is the map that ypu need to populate: it is already instantiated but empty
     *                  (in order to avoid the instantiation in each implementation of this abstract method)
     * @return
     */
    protected abstract HashMap<TInput, TOutput> populateMap(HashMap<TInput, TOutput> emptyMap);

    /**
     * Return an instance of the class identified by the "instanceType" parameter.
     *
     * @param instanceType: Key of the instance to be created. This key must be in the internal map
     * @return
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     */
    public TOutput createInstance(TInput instanceType) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        if (!_map.containsKey(instanceType))
            throw new IllegalArgumentException(
                    String.format("Class %s is not managed by the factory", instanceType.toString()));

        var type = _map.get(instanceType);
        return (TOutput) type.getClass().getDeclaredConstructor().newInstance();
    }


    //--------------- region Instance private methods ---------------

    private HashMap<TInput, TOutput> initMap() {
        //Purpose of this method is just the map instantiation
        var result = new HashMap<TInput, TOutput>();
        return populateMap(result);
    }

    //endregion

}
